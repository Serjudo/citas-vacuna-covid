package com.techu.backend.repository;

import com.techu.backend.model.CentroModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CentroRepository extends MongoRepository<CentroModel, String> {
}
