package com.techu.backend.repository;

import com.techu.backend.model.CitaDispModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CitaDispRepository extends MongoRepository<CitaDispModel, String> {
    public List<CitaDispModel> findByCentroid(String centroid);
}
