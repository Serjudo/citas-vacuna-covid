package com.techu.backend.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.text.ParseException;
import java.util.Date;

@Document(collection = "reservas")
public class ReservaModel {
    @Id
    @NotNull
    private String dni;
    @NotNull
    private String nombre;
    @NotNull
    private String apellidos;
    private Integer edad;
    private String sexo;
    @NotNull
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date dia;
    @NotNull
    private String hora;
    @NotNull
    private String centroid;
    private String citadispid;

    public ReservaModel() throws ParseException {}

    public ReservaModel(@NotNull String dni, @NotNull String nombre, @NotNull String apellidos, Integer edad, String sexo, @NotNull Date dia, @NotNull String hora, @NotNull String centroid, String citadispid) {
        this.dni = dni;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.edad = edad;
        this.sexo = sexo;
        this.dia = dia;
        this.hora = hora;
        this.centroid = centroid;
        this.citadispid = citadispid;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Date getDia() {
        return dia;
    }

    public void setDia(Date dia) {
        this.dia = dia;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getCentroid() {
        return centroid;
    }

    public void setCentroid(String centroid) {
        this.centroid = centroid;
    }

    public String getCitadispid() {
        return citadispid;
    }

    public void setCitadispid(String citadispid) {
        this.citadispid = citadispid;
    }
}
