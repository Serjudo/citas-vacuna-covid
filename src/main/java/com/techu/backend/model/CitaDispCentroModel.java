package com.techu.backend.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;

import java.util.Date;
import java.util.Optional;

public class CitaDispCentroModel {

    @Id
    @NotNull
    private String id;
    @NotNull
    private String centroid;
    @NotNull
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date dia;
    @NotNull
    private String hora;
    private String nombrecentro;

    public CitaDispCentroModel() {}

    public CitaDispCentroModel(@NotNull String id, @NotNull String centroid, @NotNull Date dia, @NotNull String hora, String nombrecentro) {
        this.id = id;
        this.centroid = centroid;
        this.dia = dia;
        this.hora = hora;
        this.nombrecentro = nombrecentro;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCentroid() {
        return centroid;
    }

    public void setCentroid(String centroid) {
        this.centroid = centroid;
    }

    public Date getDia() {
        return dia;
    }

    public void setDia(Date dia) {
        this.dia = dia;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getNombrecentro() {
        return nombrecentro;
    }

    public void setNombrecentro(String nombrecentro) {
        this.nombrecentro = nombrecentro;
    }
}