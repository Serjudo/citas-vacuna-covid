package com.techu.backend.controller;

import com.techu.backend.model.CitaDispCentroModel;
import com.techu.backend.model.CitaDispModel;
import com.techu.backend.service.CitaDispMongoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("${url.base}")
@CrossOrigin(origins ="*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE})

public class CitaDispController {

    @Autowired
    private CitaDispMongoService citaDispMongoService;


    //GET Todas las citas disponibles (collection)
    @GetMapping("/citasdisp")
    public List<CitaDispModel> getCitas() {
        return citaDispMongoService.findAll();
    }

    //GET Todas las citas disponibles (collection)
    @GetMapping("/citasdispall")
    public List<CitaDispModel> getCitasAll(@RequestBody CitaDispModel json) {
        return citaDispMongoService.findAllCitas(json);
    }

    //GET Todas las citas disponibles (collection)
    @GetMapping("/citasdispfecha")
    public List<CitaDispModel> getCitasFechaMayor(@RequestBody CitaDispModel dia) {
        return citaDispMongoService.findFechaMayor(dia);
    }

    //GET citas por centro_Id (instancia)
    @GetMapping("/citasdisp/{centroid}")
    public List<CitaDispModel> getCitasById(@PathVariable String centroid) {
        return citaDispMongoService.findByCentroid(centroid);
    }

    //POST para crear una cita disponible
    @PostMapping("/citasdisp")
    public CitaDispModel postCita(@RequestBody CitaDispModel nuevaCita) {
        citaDispMongoService.save(nuevaCita);
        return nuevaCita;
        //return new ResponseEntity<String>("cita creada correctamente", HttpStatus.CREATED);
    }

    //DELETE una cita por el id
    @DeleteMapping("/citasdisp/{id}")
    public boolean deleteCitaById(@PathVariable String id) {
        return citaDispMongoService.deleteById(id);
    }

}
