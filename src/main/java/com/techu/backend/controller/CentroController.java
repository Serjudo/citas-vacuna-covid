package com.techu.backend.controller;

import com.techu.backend.model.CentroModel;
import com.techu.backend.service.CentroMongoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("${url.base}")
@CrossOrigin(origins ="*", methods = {RequestMethod.GET, RequestMethod.POST})
public class CentroController {

    @Autowired
    private CentroMongoService centroMongoService;

    //GET Todos los centros de salud disponibles (collection)
    @GetMapping("/centros")
    public List<CentroModel> getCentros() {
        return centroMongoService.findAll();
    }

    //GET centro por ID (instancia)
    @GetMapping("/centros/{id}")
    public Optional<CentroModel> getCentroById(@PathVariable String id) {
        return centroMongoService.findById(id);
    }

    //POST para crear un centro de salud
    @PostMapping("/centros")
    public CentroModel postReserva(@RequestBody CentroModel nuevoCentro) {
        centroMongoService.save(nuevoCentro);
        return nuevoCentro;
        //return new ResponseEntity<String>("centro creado correctamente", HttpStatus.CREATED);
    }
}
