package com.techu.backend.controller;

import com.techu.backend.model.ReservaModel;
import com.techu.backend.service.CitaDispMongoService;
import com.techu.backend.service.ReservaMongoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("${url.base}")
@CrossOrigin(origins ="*", methods = {RequestMethod.GET, RequestMethod.POST})
public class ReservaController {

    @Autowired
    private ReservaMongoService reservaMongoService;

    @Autowired
    private CitaDispMongoService citaDispMongoService;

    @GetMapping("")
    public String index() {
        return "API REST Vacunas COVID v1.0.0";
    }

    //GET Todas las reservas realizadas (collection)
    @GetMapping("/reservas")
    public List<ReservaModel> getReservas() {
        return reservaMongoService.findAll();
    }

    //POST para crear una reserva
    @PostMapping("/reservas")
    public ResponseEntity<String> postReserva(@RequestBody ReservaModel nuevaReserva) throws ParseException {
        reservaMongoService.save(nuevaReserva);
        String citaid = nuevaReserva.getCitadispid();
        citaDispMongoService.deleteById(citaid);
        //return nuevaReserva;
        return new ResponseEntity<String>("Reserva creada correctamente", HttpStatus.CREATED);
    }
}