package com.techu.backend.service;

import com.techu.backend.model.CentroModel;
import com.techu.backend.model.CitaDispCentroModel;
import com.techu.backend.model.CitaDispModel;
import com.techu.backend.repository.CentroRepository;
import com.techu.backend.repository.CitaDispRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.mongodb.core.MongoOperations;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class CitaDispMongoService {
    @Autowired
    CitaDispRepository citaDispRepository;

    @Autowired
    MongoOperations mongoOperations;

    //READ Todas las citas disponibles
    public List<CitaDispModel> findAll() {
        return citaDispRepository.findAll();
    }

    //READ Todas las citas por centro_id
    public List<CitaDispModel> findByCentroid(String centroid) {
        return citaDispRepository.findByCentroid(centroid);
    }

    //READ Todas las citas mayores a una fecha
    public List<CitaDispModel> findFechaMayor(CitaDispModel dia) {
        List<CitaDispModel> citasActual = citaDispRepository.findAll();
        List<CitaDispModel> citasFinal = new ArrayList<>();
        for (int i=0; i < citasActual.size(); i++) {
            if (citasActual.get(i).getDia().getTime() >= dia.getDia().getTime()) {
                citasFinal.add(citasActual.get(i));
            }
        }
        return citasFinal;
    }

    //READ Todas las citas con todos los datos
    public List<CitaDispModel> findAllCitas(CitaDispModel json) {
        if (json.getCentroid()== null & json.getDia()== null) {
            return null;
        }

        if (json.getCentroid() == null) {
            List<CitaDispModel> citasActual = citaDispRepository.findAll();
            List<CitaDispModel> citasFinal = new ArrayList<>();

            //CentroMongoService centroMongoService = new CentroMongoService();

            for (int i=0; i < citasActual.size(); i++) {
                //CitaDispCentroModel cd = new CitaDispCentroModel();
                if (citasActual.get(i).getDia().getTime() >= json.getDia().getTime()) {

                    /* cd.setId(citasActual.get(i).getId());
                    cd.setCentroid(citasActual.get(i).getCentroid());
                    cd.setDia(citasActual.get(i).getDia());
                    cd.setHora(citasActual.get(i).getHora());
                    cd.setNombrecentro(centroMongoService.findOne(citasActual.get(i).getCentroid()).getNombre());
                    citasFinal.add(cd); */

                    citasFinal.add(citasActual.get(i));
                }
            }
            return citasFinal;
        } else if (json.getDia() == null) {
            return citaDispRepository.findByCentroid(json.getCentroid());
        } else {
            List<CitaDispModel> citasActual = citaDispRepository.findByCentroid(json.getCentroid());
            List<CitaDispModel> citasFinal = new ArrayList<>();
            for (int i=0; i < citasActual.size(); i++) {
                if (citasActual.get(i).getDia().getTime() >= json.getDia().getTime()) {
                    citasFinal.add(citasActual.get(i));
                }
            }
            return citasFinal;
        }
    }

    //CREATE cita disponible
    public CitaDispModel save(CitaDispModel nuevaCita) {
        citaDispRepository.save(nuevaCita);
        return nuevaCita;
    }

    //DELETE una cita disponible por el ID
    public boolean deleteById(String id) {
        try {
            citaDispRepository.deleteById(id);
            return true;
        } catch(Exception ex) {
            return false;
        }
    }
}
