package com.techu.backend.service;

import com.techu.backend.model.ReservaModel;
import com.techu.backend.repository.ReservaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class ReservaMongoService {

    @Autowired
    ReservaRepository reservaRepository;

    //READ
    public List<ReservaModel> findAll() {
        return reservaRepository.findAll();

    }

    //CREATE
    public ReservaModel save(ReservaModel newReserva) throws ParseException {
        reservaRepository.save(newReserva);
        return newReserva;

    }
}
