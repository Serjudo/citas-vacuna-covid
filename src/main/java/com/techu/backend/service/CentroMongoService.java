package com.techu.backend.service;

import com.techu.backend.model.CentroModel;
import com.techu.backend.repository.CentroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CentroMongoService {

    @Autowired
    CentroRepository centroRepository;

    @Autowired
    MongoOperations mongoOperations;

    //READ Todos los centros
    public List<CentroModel> findAll() {
        return centroRepository.findAll();

    }

    //READ by Centro ID
    public Optional<CentroModel> findById(String id) {
        return centroRepository.findById(id);
    }

    public CentroModel findOne(String id) {
        return mongoOperations.findOne(new Query(Criteria.where("id").is(id)), CentroModel.class);
    }

    //CREATE Centro
    public CentroModel save(CentroModel newCentro) {
        centroRepository.save(newCentro);
        return newCentro;

    }
}
